# Luminous Enterprises Solar Panel Array

The Luminous Enterprises Solar Panel Array significantly increases the
battery charge rate of a ship.

Since it is a passive upgrade, no configuration or activation is
necessary to take advantage of its functionality.

Copyright © 2433 Luminous Enterprises, All Rights Reserved.
