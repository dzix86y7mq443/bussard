# Bencode Decoder

Thanks for purchasing the Bencode Decoder! Using it is very easy.

Simply run `ship.actions.bencode_decoder(bencoded_data_string)` and it will
return the decoded data structure. If the string is malformed it will raise an
error.

Copyright © 2402 Gropos Software, All Rights Reserved
