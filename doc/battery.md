# Luminous Enterprises Onboard Battery Upgrade

The Luminous Enterprises Onboard Battery Upgrade significantly increases the
battery capacity of a ship on which it is installed.

Since it is a passive upgrade, no configuration or activation is
necessary to take advantage of its functionality.

Copyright © 2417 Luminous Enterprises, All Rights Reserved.
