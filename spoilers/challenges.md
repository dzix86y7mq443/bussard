# Challenges

The first has been implemented; the rest are just notes for possibilities.

## Escape the initial system

You awaken in orbit with dex-19 having reactivated you. He wants to get out of
the system, and so do you, so you can work together.

* Objective: pass thru the portal
 * Need to read dex-19 email (skill: operating editor)
 * Restart engines (skill: editor/console)
 * Fly to Merdeka station (skill: piloting)
 * Log in and take the CMEC training course (skill: ssh, basic lua)
 * Buy the battery upgrade (skill: ssh, orb)

## Decode log

After making it to Tana, dex-19 informs you that he's found a memory card
wedged in the cargo bay, but it's an older style that he doesn't have the
hardware to read. He suggests paying a visit to an old friend who can help
read it. The friend is a bit of a recluse, so you need to land a rover to
bring the memory card to them.

* Objective: read log file
 * Fly to Solotogo(?) (skill: piloting)
 * Navigate the rover to the dwelling (skill: rover navigation)
 * Once you have the file, decode the rot13 (skill: lua)

Need to figure out how you get clearance to land the rover here.

## Unlock interportal

I think it makes sense to limit the game to the Tana systems as you're just
getting started so you don't get disoriented or too far off track.

* Objective: gain access to the interportal to Sol
 * ???

## Unlock the spacetime junction

All systems are open to you here, except Katilay?

dex-19 tells you that you're a unique construct in that you have access to a
spacetime junction, which functions as a kind of save point. In order to find
out how to activate it, you need to track down some docs.

Rocanna is the one who knows how the junction works, but she doesn't trust
Traxus and can't be told why you want to activate it?

* Objective: learn how to reset the game state
 * Find documentation, somewhere in Bohk?
 * Create the junction point (skill: forth programming)
 * Activate the junction point (skill: forth programming)

Once you activate it for the first time, dex-19 notices that it's working, and
he tells you of the plan to reactivate Traxus.

## Reactivate Traxus

* Objective: reactivate Traxus's base on Sutep
 * Find the location of the base (skill: map?)
 * Land a rover there (skill: rover deploy)
 * Bring the reactor online (skill: lisp/forth programming?)
 * Subvert security measures to reach Traxus (skill: rover movement)
 * Fix communications array, connecting Traxus to network

Once you've brought the reactor online, the whole time you're working to
connect him to the network he is talking to you in ways that make you more and
more uncomfortable with the idea of reactivating him.

Once you bring him online he begins his plan of making the humans pay. Realize
that was a terrible mistake, activate spacetime junction to reset.

## Stop Traxus's followers and that other guy

Katilay portal access is limited due to damage.

* Somehow get back to Katilay
 * Gain creds for the portal thru password logger (skill: lua programming)
 * Log in and activate the portal (skill: lisp programming)

Reset the junction so you can return to this point.

Ultimate showdown here!

# Skills

* piloting
* SSH
* email

* console
* editor

* deploying rover
* rover console
* rover coding
* map

* sql?
* programming
 * lua
 * lisp
 * forth
 * lily?
