-- Edit mode -*- lua -*-

dofile("src.edit_functions")

define_mode("edit")

-- most of the functions these are bound to should be self-explanatory
-- just by looking at the function name.

local exit = function()
   editor.save()
   editor.change_buffer("*flight*")
end

bind("edit", "escape", exit)
bind("edit", "ctrl-return", exit)
bind("edit", "ctrl-o", editor.find_file)

bind({"edit", "minibuffer"}, "ctrl-q", function()
      editor.save()
      ship.ui.quit()
end)

-- cycle forwards and backwards through open buffers.
bind("edit", "ctrl-pageup", lume.fn(editor.next_buffer, -1))
bind("edit", "ctrl-pagedown", editor.next_buffer)

-- not all keyboards have these keys, but they're handy.
bind("edit", "appback", lume.fn(editor.next_buffer, -1))
bind("edit", "appforward", editor.next_buffer)

-- you can bind return to just editor.newline to disable auto-indent.
bind("edit", "return", editor.newline_and_indent)
bind("edit", "ctrl-z", editor.undo)
bind("edit", "ctrl-s", editor.save)
bind("edit", "alt-r", editor.revert)

-- "Conventional" keys
bind("edit", "backspace", editor.delete_backwards)
bind("edit", "delete", editor.delete_forwards)

bind("edit", "home", editor.beginning_of_line)
bind("edit", "end", editor.end_of_line)
bind("edit", "left", editor.backward_char)
bind("edit", "right", editor.forward_char)
bind("edit", "up", editor.prev_line)
bind("edit", "down", editor.next_line)
bind("edit", "wheelup", editor.prev_line)
bind("edit", "wheeldown", editor.next_line)

bind("edit", "ctrl- ", editor.mark)
bind("edit", "ctrl-space", editor.mark)
bind("edit", "ctrl-c", editor.kill_ring_save)
bind("edit", "ctrl-x", editor.kill_region)
bind("edit", "ctrl-v", editor.yank)

bind("edit", "alt-v", editor.system_yank)
bind("edit", "alt-c", editor.system_copy_region)

bind("edit", "pageup", editor.scroll_up)
bind("edit", "pagedown", editor.scroll_down)

bind("edit", "ctrl-home", editor.beginning_of_buffer)
bind("edit", "ctrl-end", editor.end_of_buffer)

bind("edit", "ctrl-w", editor.close)
bind("edit", "ctrl-alt-b", editor.switch_buffer)

-- force close even if unsaved changes exist.
bind("edit", "ctrl-alt-w", function()
        editor.close(true)
end)

-- Search
bind("edit", "ctrl-f", editor.search)
bind("edit", "alt-r", editor.replace)

bind("edit", "alt-g", function()
        editor.read_line("Go to line: ", function(l, cancel)
                            if(not cancel) then
                               editor.go_to(tonumber(l)) end end)
end)

-- uncomment this out to enable Emacs key bindings, which conflict with
-- some of the more conventional bindings.
-- dofile("src.emacs")
